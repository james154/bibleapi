//
//  BookChapterVerse.swift
//  BibleScribe
//
//  Created by James Sumners on 1/23/20.
//  Copyright © 2020 James Sumners. All rights reserved.
//

import Foundation

class BookChapterVerse {
    
    
    
    
//    enum BIBLE_APP: Int, CaseIterable {
//        case LOGOS
//        case OLIVETREE
//        case YOU_VERSION
//        case THE_HOLY_BIBLE
//
//        var description: String {
//            switch self {
//            case .LOGOS:
//                return "Logos"
//            case .OLIVETREE:
//                return "Olivetree"
//            case .YOU_VERSION:
//                return "YouVersion App"
//            case .THE_HOLY_BIBLE:
//                return "Holy Bible App"
//            }
//        }
//
//    }
    
    struct BibleReferences {
        var range = NSMakeRange(0, 1)
        var book = ""
        var bookId = 0
        var chapter = 0
        var verse = 0
        func reference() -> String {
            return "\(book) \(chapter):\(verse)"
        }
        func link(withBibleApp: BibleReference.BIBLE_APP) -> String
        {
            var returnLink: String = ""
            let youVersion = YouVersion(string: self.book)
            
            if (withBibleApp == .LOGOS)
            {
                returnLink = "logosref:Bible."
                let book = self.book.replacingOccurrences(of: "\\s", with: "", options: .regularExpression)
                returnLink = returnLink + book + "." + String(self.chapter) + "." + String(self.verse)
            }
            else if (withBibleApp == .OLIVETREE)
            {
                let tempStr = "olivetree://bible/"+"\(self.book) \(self.chapter):\(self.verse)".addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!
                returnLink = tempStr
            }
            else if (withBibleApp == .YOU_VERSION)
            {
                returnLink = "youversion://bible?&reference="
                let book = youVersion.abbreviation//self.book.replacingOccurrences(of: "\\s", with: "", options: .regularExpression)
                returnLink = returnLink + book + "." + String(self.chapter) + "." + String(self.verse)
            }
            else if (withBibleApp == .BIBLE_API)
            {
                let book = youVersion.abbreviation//self.book.replacingOccurrences(of: "\\s", with: "", options: .regularExpression)
                returnLink = book + "." + String(self.chapter)
            }
            else if (withBibleApp == .THE_HOLY_BIBLE)
            {
                returnLink = "thba://"
                let book = self.bookId
                returnLink = returnLink + String(book) + "." + String(self.chapter) + "." + String(self.verse)
            }
            
            if true
            {
                print(returnLink)
            }
            
            return returnLink
        }
    }
    
    
    let bookRegex = "(([1-3]\\s)?([a-zA-Z]+))\\s?"
    let versesRegex = "((\\d+)((:\\d+)+)?([-–]\\d+)?([,;](\\s)?)?)+"
    let verseRegex = "(\\d+)((:\\d+)+)?([-–]\\d+)?"
    
    private var inputString: String
    private var bookref = " "
    private var bookRef: [Int]?
    public var isRef = false
    public var references: [NSRange : String]?
    private var resetReferenceProcess = true
    
    var bibleReferences = [BibleReferences()]
    
    init(string: String){
        inputString = string.removingPercentEncoding ?? string
        isBibleReference()
        bibleReferences.remove(at: 0)
    }
    
    func isBibleReference() {
        let refPattern = "\(bookRegex)\(versesRegex)"
        let regexRef = try? NSRegularExpression(pattern: refPattern, options: .caseInsensitive)
        let attrRef = NSMutableAttributedString(string: inputString)
        
        if let refMatch = regexRef?.matches(in: inputString, options: [], range: NSMakeRange(0, inputString.count)) {
            for ref in refMatch {
                resetReferenceProcess = true
                let processRef = attrRef.mutableString.range(of: bookRegex, options: .regularExpression, range: ref.range)
                if isBook(name: processRef)
                {
                    let bk = attrRef.attributedSubstring(from: processRef).string.trimmingCharacters(in: .whitespacesAndNewlines)
                    print("Book: \(bk)")
                    
                    let regexVerses = try? NSRegularExpression(pattern: verseRegex, options: .caseInsensitive)
                    let verseRange = NSMakeRange(processRef.location+processRef.length, ref.range.length-processRef.length)
                    if let verMatch = regexVerses?.matches(in: inputString, options: [], range: verseRange)
                    {
                        var firstVerse = true
                        for ver in verMatch {
                            var foundReference = chapterVerse(book: bk, verseReference: attrRef.attributedSubstring(from: ver.range).string)
                            var range = ver.range
                            if firstVerse
                            {
                                firstVerse = false
                                range = NSMakeRange(processRef.location, processRef.length+ver.range.length)
                            }
                            
                            foundReference.range = range
                            
                            if foundReference.chapter != 0
                            {
                                bibleReferences.append(foundReference)
                            }
                            resetReferenceProcess = false
                        }
                        
                    }
                }
            }
            
        }
    }
    
    
    func isBook(name: NSRange) -> Bool {
        let testStr = NSAttributedString(string: inputString).attributedSubstring(from: name)
        let bookName = testStr.string.trimmingCharacters(in: .whitespacesAndNewlines)
        let ref = BibleReference()
        
        bookRef = ref.bookVerse[bookName]
        let validBook = (bookRef != nil)
        
        return validBook
        
    }
    
    func chapterVerse(book: String, verseReference: String) -> BibleReferences {
        let bookName = book.trimmingCharacters(in: .whitespacesAndNewlines)
        let chapterBreak = verseReference.split(separator: ":")
        var ch = 1
        var ver = 1
        var getChNotVer = false
        var invalidReference = false
        let ref = BibleReference()
        
        print("Chapter Break: \(chapterBreak.count)")
        for i in 0 ..< chapterBreak.count {
            if i == 0 && chapterBreak.count > 1
            {
                print("Chapter: \(chapterBreak[i])")
                if let chapter = Int(String(chapterBreak[i]))
                {
                    if chapter <= bookRef?.count ?? 0
                    {
                        ch = chapter
                        print("Valid chapter: \(chapter) of \(bookRef!.count)")
                    }
                    else
                    {
                        invalidReference = true
                    }
                }
            }
            else
            {
                if chapterBreak.count == 1
                {
                    if bibleReferences.count > 1
                    {
                        // Here is the failure to get the correct chapter...
                        if resetReferenceProcess == false && bibleReferences[bibleReferences.count-1].book == book
                        {
                            ch = bibleReferences[bibleReferences.count-1].chapter
                        }
                        else
                        {
                            if ref.bookVerse[bookName]!.count > 1
                            {
                                getChNotVer = true
                            }
                        }
                    }
                }
                print("Verse: \(chapterBreak[i])")
                let verseBreak = chapterBreak[i].split(separator: "-")
                
                if let verse = Int(String(verseBreak[0]))
                {
                    print("\(verse)")
                    if (getChNotVer)
                    {
                        if verse <= bookRef?.count ?? 0
                        {
                            ch = verse
                            print("Valid chapter: \(verse) of \(bookRef!.count)")
                        }
                        else {
                            invalidReference = true
                        }
                    }
                    else {
                        let numVerses = bookRef![ch-1]
                        print(numVerses)
                        if verse <= numVerses
                        {
                            ver = verse
                        }
                        else
                        {
                            invalidReference = true
                        }
                    }
                }
                
            }
        }
        if invalidReference == true
        {
            ch = 0
            ver = 0
        }
        
        return BibleReferences(book: book, bookId: ref.bookNumber[book].unsafelyUnwrapped, chapter: ch, verse: ver)
        
    }
    
}
