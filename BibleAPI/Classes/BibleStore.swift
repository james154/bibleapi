//
//  BibleStore.swift
//  BibleAPI
//
//  Created by James Sumners on 12/10/20.
//

import SwiftUI


class BibleStore: ObservableObject {
    
    @Published var language: String = "eng"
    @Published var translation: BibleVersion = UserDefaults.standard.value(forKey: "BibleVersion") as? BibleVersion ?? BibleVersion(id: "06125adad2d5898a-01", abbreviation: "ASV", name: "American Standard")
    {
        didSet {
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(translation) {
                print("Setting Version: \(translation.name)")
                let defaults = UserDefaults.standard
                defaults.set(encoded, forKey: "BibleVersion")
            }
            else {
                print("COULD NOT ENCODE TRANSLATION!")
            }
        }
    }
    @Published var books: BibleBooks = UserDefaults.standard.value(forKey: "BibleBooks") as? BibleBooks ?? BibleBooks()
    {
        didSet {
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(books) {
                print("Number of Books: \(books.list.count)")
                let defaults = UserDefaults.standard
                defaults.set(encoded, forKey: "BibleBooks")
            }
            else {
                print("COULD NOT ENCODE BOOK!")
            }
        }
    }
    @Published var book: BibleBook = UserDefaults.standard.value(forKey: "BibleBook") as? BibleBook ?? BibleBook(id: "GEN", abbreviation: "Gen.", name: "Genesis", chapters: 50) {
        didSet {
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(book) {
                print("Setting Book: \(book.name)")
                let defaults = UserDefaults.standard
                defaults.set(encoded, forKey: "BibleBook")
            }
            else {
                print("COULD NOT ENCODE BOOK!")
            }
        }
    }
    @Published var chapter: Int = UserDefaults.standard.integer(forKey: "Chapter") as Int {
        didSet {
            print("Setting Chapter: \(chapter)")
            UserDefaults.standard.set(chapter, forKey: "Chapter")
        }
    }
    @Published var chapters: Int = 50
    @Published var versions: [BibleVersion] = [BibleVersion(id: "06125adad2d5898a-01", abbreviation: "ASV", name: "American Standard")]
    @Published var passage = NSAttributedString()
    @Published var title = "Bible Passage"
    @Published var apiKey: String
    @Published var searchTerm: String = ""
    @Published var searchResults: [SearchResult] = [SearchResult]()
    @Published var fontName: String = UserDefaults.standard.string(forKey: "FontName") ?? "Baskerville" {
        didSet {
            print("Setting FontName: \(fontName)")
            UserDefaults.standard.set(fontName, forKey: "FontName")
        }
    }
    @Published var fontSize: Int = UserDefaults.standard.integer(forKey: "FontSize") as Int {
        didSet {
            print("Setting FontSize: \(fontSize)")
            UserDefaults.standard.set(fontSize, forKey: "FontSize")
        }
    }
    @Published var verseSize: Int = UserDefaults.standard.integer(forKey: "VerseSize") as Int {
        didSet {
            print("Setting VerseSize: \(verseSize)")
            UserDefaults.standard.set(verseSize, forKey: "VerseSize")
        }
    }
    
    var htmlPassage = ""
    
    var font: UIFont {
        get {
            return UIFont(name: self.fontName, size: CGFloat(fontSize))!
        }
        set {
            self.fontName = newValue.fontName
            self.fontSize = Int(newValue.pointSize)
        }
    }
    
    init(lang: String, key: String) {
        language = lang
        apiKey = key
        loadBooks()
        print("Current Chapter \(chapter)")
        if chapter == 0 {
            chapter = 1
        }
        
        if fontSize == 0 {
            fontSize = 22
        }
        if verseSize == 0 {
            verseSize = 15
        }
        
    }

    func loadBooks() {
        guard let url = URL(string: "https://api.scripture.api.bible/v1/bibles/\(translation.id)/books?include-chapters=true") else {
            print("Invalid URL")
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue(apiKey, forHTTPHeaderField: "api-key")
        
        //        let session = URLSession(configuration: .default)
        URLSession.shared.dataTask(with: request) { data, response, error in
            if let content = data {
                
                DispatchQueue.main.async {
                    var tempBooks = [BibleBook]()
                    
                    do {
                        let jsonResult = try JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                        
                        // Parse JSON data
                        let jsonBooks = jsonResult?["data"] as! [AnyObject]
                        for jsonBook in jsonBooks {
                            var bk = BibleBook()
                            bk.name = jsonBook["name"] as! String
                            bk.abbreviation = jsonBook["abbreviation"] as! String
                            bk.id = jsonBook["id"] as! String
                            let testChArray = jsonBook["chapters"] as! [NSDictionary]
                            
                            bk.chapters =  testChArray.count - 1
                            tempBooks.append(bk)
                        }
                        
                        
                        if self.books.list.count != tempBooks.count {
                            
                            if tempBooks.contains(self.book) == false {
                                self.book = tempBooks[0]
                            }
                            self.books.list.removeAll()
                            
                            for bk in tempBooks {
                                self.books.list.append(bk)
                            }
                        }
                        
                        
                    } catch {
                        print("Fetch failed: \(error.localizedDescription )")
                    }
                }
            }
            
            // if we're still here it means there was a problem
            print("Loading Books")
        }.resume()
        
        
    }
    
    func loadVersions() {
        guard let url = URL(string: "https://api.scripture.api.bible/v1/bibles?language=\(language)") else {
            print("Invalid URL")
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue(apiKey, forHTTPHeaderField: "api-key")
        
        //        let session = URLSession(configuration: .default)
        URLSession.shared.dataTask(with: request) { data, response, error in
            if let content = data {
                
                DispatchQueue.main.async {
                    var tempVersions = [BibleVersion]()
                    
                    do {
                        let jsonResult = try JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                        
                        // Parse JSON data
                        let jsonVersions = jsonResult?["data"] as! [AnyObject]
                        for jsonVersion in jsonVersions {
                            var vers = BibleVersion()
                            vers.name = jsonVersion["name"] as! String
                            vers.abbreviation = jsonVersion["abbreviationLocal"] as! String
                            vers.id = jsonVersion["id"] as! String
                            vers.description = jsonVersion["nameLocal"] as! String
                            tempVersions.append(vers)
                        }
                        
                        if self.versions.count != tempVersions.count {
                            self.versions.removeAll()
                            for vers in tempVersions {
                                self.versions.append(vers)
                            }
                        }
                        
                    } catch {
                        print("Fetch failed: \(error.localizedDescription )")
                    }
                    
                    
                }
            }
            
            // if we're still here it means there was a problem
            print("Loading Versions")
        }.resume()
        
        
    }
    
    func chapterInRange(_ bkIndex: String, _ chIndex: Int) -> Int {
        for bk in self.books.list {
            if bk.id == bkIndex && chIndex <= bk.chapters {
                self.chapter = chIndex
                return chIndex
            }
        }
        
        self.chapter = 1
        return self.chapter
    }
    
    func loadScripture()
    {
        let testCh = chapterInRange(book.id, chapter)
        guard let url = URL(string: "https://api.scripture.api.bible/v1/bibles/\(translation.id)/passages/\(book.id).\(testCh)?content-type=html&include-notes=false&include-titles=false&include-chapter-numbers=false&include-verse-numbers=true&include-verse-spans=false&use-org-id=false") else {
            print("Invalid URL")
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue(apiKey, forHTTPHeaderField: "api-key")
        
        //        let session = URLSession(configuration: .default)
        URLSession.shared.dataTask(with: request) { data, response, error in
            if let content = data {
                
                DispatchQueue.main.async {
                    
                    do {
                        let biblePassage: BibleJSON = try JSONDecoder().decode(BibleJSON.self, from: content)
                        
                        var tempString = """
<style>
                        span.v {
                          color: #000;
                          font-size: \(self.verseSize)px;
                          letter-spacing: 0.07em;
                          vertical-align: 0.40em;
                          font-family: \(self.fontName);
                        }
                        p {
                          color: #000;
                          font-size: \(self.fontSize)px;
                          font-family: \(self.fontName);
                        }
                      </style>
"""
                        tempString +=  biblePassage.data.content
                        
                        self.htmlPassage = biblePassage.data.content
                        
                        if let temp = try? NSAttributedString(data: Data(tempString.utf8), options: [.documentType: NSAttributedString.DocumentType.html,.characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil) {
                            self.passage = temp
                            
                            self.title = biblePassage.data.reference
                        }
                    } catch {
                        print("Fetch failed: \(error.localizedDescription )")
                        print("\(self.translation.abbreviation) - \(self.book.name) - \(self.chapter)")
                    }
                }
            }
            
            // if we're still here it means there was a problem
            print("Loading Scripture")
        }.resume()
        
    }
    
    func updateScripture() {
        DispatchQueue.main.async {
            print("updateScripture")
            
            var tempString = """
<style>
        span.v {
          color: #000;
          font-size: \(self.verseSize)px;
          letter-spacing: 0.07em;
          vertical-align: 0.40em;
          font-family: \(self.fontName);
        }
        p {
          color: #000;
          font-size: \(self.fontSize)px;
          font-family: \(self.fontName);
        }
      </style>
"""
            tempString +=  self.htmlPassage
            
            print(tempString)
            
            if let temp = try? NSAttributedString(data: Data(tempString.utf8), options: [.documentType: NSAttributedString.DocumentType.html,.characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil) {
                self.passage = temp
            }
        }
    }
    
    func performSearch()
    {
        guard let url = URL(string: "https://api.scripture.api.bible/v1/bibles/\(translation.id)/search?query=\(searchTerm)&sort=canonical&limit=1000") else {
            print("Invalid URL")
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue(apiKey, forHTTPHeaderField: "api-key")
        
        //        let session = URLSession(configuration: .default)
        URLSession.shared.dataTask(with: request) { data, response, error in
            if let content = data {
                
                DispatchQueue.main.async {
                    
                    do {
                        var tempResult = [SearchResult]()
                    let jsonResult = try JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                    
                    // Parse JSON data
                    let jsonVersions = jsonResult?["data"] as? NSDictionary
                        if let versesReturned = jsonVersions?["verses"] as? [AnyObject] {
                            for verseItem in versesReturned {
                                var item = SearchResult()
                                item.id = verseItem["id"] as! String
                                item.bibleId = verseItem["bibleId"] as! String
                                item.chapterId = verseItem["chapterId"] as! String
                                item.reference = verseItem["reference"] as! String
                                item.text = verseItem["text"] as! String
                                
                                tempResult.append(item)
                            }
                        }
                        self.searchResults.removeAll()
                        self.searchResults = tempResult
                    } catch {
                        print("error")
                    }
                }
            }
            
            // if we're still here it means there was a problem
            print("Loading Scripture")
        }.resume()
        
    }
    
    func getBook(with id: String) -> BibleBook {
        var retValue = books.list[0]
        
        for bk in books.list {
            if bk.id == id {
                retValue = bk
                break
            }
        }
        
        return retValue
    }
    
    func getVersion(with id: String) -> BibleVersion {
        var retValue = versions[0]
        
        for ver in versions {
            if ver.id == id {
                retValue = ver
                break
            }
        }
        
        return retValue
    }
    
    func setBookChapter(from string: String) {
        let refComponents = string.components(separatedBy: ".")
        
        book = getBook(with: refComponents[0])
        chapter = Int(refComponents[1]) ?? 1
    }
}
