//
//  VersePickerView.swift
//  BibleAPI
//
//  Created by James Sumners on 11/13/20.
//

import SwiftUI

struct VersePickerCenter: View {
    @ObservedObject var bibleStore: BibleStore
    let column = [GridItem(.flexible(minimum: 150)),
                  GridItem(.flexible(), spacing: 15),
                  GridItem(.flexible(), spacing: 15)
    ]
    var body: some View {
        VStack{
            
            LazyVGrid(columns: column, content: {
                
                NavigationLink(destination: VersionPicker(bibleStore: bibleStore),
                               label: {
                                HStack {
                                Image(systemName: "books.vertical")
                                    Text(bibleStore.translation.abbreviation)
                                }
                               })
                    .padding(10)
                    .background(Color(.secondarySystemFill))
                    .cornerRadius(10)

                NavigationLink(bibleStore.book.abbreviation, destination: BookPicker(bibleStore: bibleStore))
                    
         .padding(10)
         .background(Color(.secondarySystemFill))
         .cornerRadius(10)
                
                NavigationLink("Chapter \(bibleStore.chapter)", destination: ChapterPicker(bibleStore: bibleStore))
                    
         .padding(10)
         .background(Color(.secondarySystemFill))
         .cornerRadius(10)
            })
        }
        .padding()
        .frame(height: 80, alignment: .top)
        .background(Color("bibleMenuColor"))
        .foregroundColor(Color("bibleMenuLabel"))
        
        
    }
}

enum BibleSheet {
    case none
    case version
    case book
    case chapter
    case search
    case settings
}

class SheetNavigator: ObservableObject {
    @Published var showSheet = false
    @Published var selectedSheet = BibleSheet.none {
        didSet {
            if selectedSheet != .none {
                showSheet = true
            }
            else {
                showSheet = false
            }
        }
    }
    
}

struct VersePickerSide: View {
    @ObservedObject var bibleStore: BibleStore
    @Binding var pickerAlignment: HorizontalAlignment
    @Binding var showMe: Bool
    @StateObject var activeSheet = SheetNavigator()
    
    var body: some View {
        VStack {
            if showMe {
                HStack {
                    Button(action: {
                        activeSheet.selectedSheet = .book
                    }, label: {
                        Text("\(bibleStore.book.name)")
                            .frame(minWidth: 100)
                            .padding(10)
                            .background(Color(.secondarySystemFill))
                            .cornerRadius(10)
                    })
                    
                    Button(action: {
                        activeSheet.selectedSheet = .chapter
                    }, label: {
                        Text("\(bibleStore.chapter)")
                            .frame(minWidth: 40)
                            .padding(10)
                            .background(Color(.secondarySystemFill))
                            .cornerRadius(10)
                    })
                }
                .padding(5)
                HStack {
                    Button(action: {
                        activeSheet.selectedSheet = .version
                    }, label: {
                        HStack {
                            Image(systemName: "books.vertical")
                            Text(bibleStore.translation.abbreviation)
                        }
                        .frame(minWidth: 100)
                        .padding(10)
                        .background(Color(.secondarySystemFill))
                        .cornerRadius(10)
                    })
                    
                    Button(action: {
                        activeSheet.selectedSheet = .search
                    }, label: {
                        Image(systemName: "text.magnifyingglass")
                            .frame(minWidth: 40)
                            .padding(10)
                            .background(Color(.secondarySystemFill))
                            .cornerRadius(10)
                    })
                }
                HStack {
                    if (pickerAlignment == .leading) {
                        Spacer()
                        
                        Button(action: {
                            activeSheet.selectedSheet = .settings
                        }, label: {
                            Image(systemName: "gear")
                                .frame(minWidth: 40, minHeight: 22)
                                .padding(10)
                                .background(Color(.secondarySystemFill))
                                .cornerRadius(10)
                        })
                    }
                    Button(action: {
                        if pickerAlignment == .trailing {
                            pickerAlignment = .leading
                        }
                        else if pickerAlignment == .leading {
                            pickerAlignment = .trailing
                        }
                        
                    }, label: {
                        Image(systemName: "arrow.backward")
                            .rotationEffect(pickerAlignment == .trailing ? Angle(degrees: 0) : Angle(degrees: 180))
                            
                            .frame(minWidth: 40, minHeight: 22)
                            .padding(10)
                            .background(Color(.secondarySystemFill))
                            .cornerRadius(10)
                    })
                    
                    if (pickerAlignment == .trailing) {
                        
                        Button(action: {
                            activeSheet.selectedSheet = .settings
                        }, label: {
                            Image(systemName: "gear")
                                .frame(minWidth: 40, minHeight: 22)
                                .padding(10)
                                .background(Color(.secondarySystemFill))
                                .cornerRadius(10)
                        })
                        Spacer()
                    }
                }
                .padding(5)
            }
            else {
                EmptyView()
            }
        }
        .frame(width: 200, height: 150)
        .padding(10)
        .background(Color("bibleMenuColor"))
        .foregroundColor(Color("bibleMenuLabel"))
        .cornerRadius(10)
        .sheet(isPresented: $activeSheet.showSheet, content: {
            VStack {
                getSheet()
                
                Button("Go", action: {
                    activeSheet.selectedSheet = .none
                })
                .padding(10)
                .padding(.horizontal)
                .background(Color(.secondarySystemFill))
                .cornerRadius(10)
            }
        })
    }
    
    
    func getSheet() -> AnyView {
        print("Active: \(activeSheet.selectedSheet)\nShow: \(activeSheet.showSheet)")
        if activeSheet.selectedSheet == .version {
            return VersionPicker(bibleStore: bibleStore, activeSheet: activeSheet).eraseToAnyView()
        }
        else if activeSheet.selectedSheet == .chapter {
            return BookPicker(bibleStore: bibleStore, showChapters: true, activeSheet: activeSheet).eraseToAnyView()
        }
        else if activeSheet.selectedSheet == .book {
            return BookPicker(bibleStore: bibleStore, activeSheet: activeSheet).eraseToAnyView()
        }
        else if activeSheet.selectedSheet == .search {
            return SearchView(bibleStore: bibleStore, activeSheet: activeSheet).eraseToAnyView()
        }
        else if activeSheet.selectedSheet == .settings {
            return BibleSettings(store: bibleStore).eraseToAnyView()
        }
        
        return Text("Empty").eraseToAnyView()
    }
}

extension View {
    func eraseToAnyView() -> AnyView {
        AnyView(self)
    }
}

struct VersePickerView_Previews: PreviewProvider {
    static var previews: some View {
//        VersePickerView( passage: .constant(NSAttributedString(string: "")), book:)
        Group {
            NavigationView {
                VersePickerCenter(bibleStore: BibleStore(lang: "eng", key: "52db9917134442d8559cbab5cc5e18f6"))
            }
            .preferredColorScheme(.light)
            NavigationView {
                VersePickerSide(bibleStore: BibleStore(lang: "eng", key: "52db9917134442d8559cbab5cc5e18f6"), pickerAlignment: .constant(.trailing), showMe: .constant(true))
            }
            .preferredColorScheme(.light)
        }
    }
}
