//
//  BibleAPI.swift
//  BibleAPI
//
//  Created by James Sumners on 11/16/20.
//

import SwiftUI


struct VersionPicker: View {
    @Environment(\.presentationMode) var presentationMode
    
    @ObservedObject var bibleStore: BibleStore
    @State var showBible = false
    @ObservedObject var activeSheet = SheetNavigator()
    var body: some View {
        VStack {
            
            HStack {
                Text("Version")
                    .font(.largeTitle)
                    .bold()
                    .padding()
                Spacer()
            }
            ZStack {
                Color(.secondarySystemFill)
                ScrollViewReader { proxy in
                    List {
                        ForEach(bibleStore.versions, content: { index in
                            Button(action: {
                                bibleStore.translation = index
                                print("Picked Different Version")
                                bibleStore.loadBooks()
                                activeSheet.selectedSheet = .none
//                                self.presentationMode.wrappedValue.dismiss()
                            }, label: {
                                HStack {
                                    Image(systemName: "checkmark")
                                        .foregroundColor(index.id == bibleStore.translation.id ? Color("bibleMenuLabel") : Color.clear)
                                    VStack(alignment: .leading) {
                                        Text("\(index.name)")
                                            .font(.body)
                                        Text("\(index.description)")
                                            .font(.footnote)
                                    }.id(index.id)
                                }
                            })
                        })
                    }
                    .onAppear(perform: {
                        proxy.scrollTo(bibleStore.translation.id, anchor: .center)
                    })
                }
                .cornerRadius(10)
                .padding()
            }
        }
        .navigationTitle("Bible Versions")
    }
}

struct BookPicker: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var bibleStore: BibleStore
    @State var showChapters = false
    @ObservedObject var activeSheet = SheetNavigator()
    let columns = [
        GridItem(.flexible(), spacing: 1),
        GridItem(.flexible(), spacing: 1),
        GridItem(.flexible(), spacing: 1),
        GridItem(.flexible(), spacing: 1),
        GridItem(.flexible(), spacing: 1),
        GridItem(.flexible(), spacing: 1),
        GridItem(.flexible(), spacing: 1),
        GridItem(.flexible(), spacing: 1),
        GridItem(.flexible(), spacing: 1),
        GridItem(.flexible(), spacing: 1)
    ]
    var body: some View {
        VStack {
            
                HStack {
                Text("Book")
                    .font(.largeTitle)
                    .bold()
                    .padding()
                    Spacer()
                }
            Spacer()
            LazyVGrid(columns: columns, spacing: 0) {
                ForEach(0..<bibleStore.books.list.count, content: { index in
                    Rectangle()
                        .aspectRatio(1.0, contentMode: .fit)
                        .foregroundColor(bibleStore.books.list[index].id == bibleStore.book.id ? Color.green: Color.gray)
                        .frame(height: 50)
                        .overlay(
                            // 6
                            Button(bibleStore.books.list[index].id, action: {
                                bibleStore.book = bibleStore.books.list[index]
                                showChapters = true
                            })
                            .font(.system(.body))
                            .foregroundColor(.white)
                        )
                })
            }
            
            if showChapters {
                ChapterOption(bibleStore: bibleStore, displayAlone: false)
                    .transition(.move(edge: .bottom))
                    .animation(.easeInOut)
            }
        }
        
            .transition(.move(edge: .bottom))
            .animation(.easeInOut)
        .onChange(of: bibleStore.book, perform: { value in
            for bk in bibleStore.books.list {
                if bk.id == value.id {
                    bibleStore.chapters = bk.chapters
                }
            }
            
            bibleStore.chapter = 1
        })
//        .onChange(of: bibleStore.chapter, perform: { value in
//            if bibleStore.chapter != value {
//                self.presentationMode.wrappedValue.dismiss()
//            }
//        })
        
    }
}

struct ChapterOption: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var bibleStore: BibleStore
    @State var displayAlone = true
    @ObservedObject var activeSheet = SheetNavigator()
    
    var body: some View {
        VStack {
            if displayAlone {
                    HStack {
                    Text("Chapter")
                        .font(.largeTitle)
                        .bold()
                        .padding()
                        Spacer()
                    }
                Spacer()
            }
            if bibleStore.chapters > 70 {
                ScrollView {
                    ChapterPicker(bibleStore: bibleStore, displayAlone: displayAlone, activeSheet: activeSheet)
                }
            }
            else {
                ChapterPicker(bibleStore: bibleStore, displayAlone: displayAlone, activeSheet: activeSheet)
            }
        }
        .onChange(of: bibleStore.chapter, perform: { value in
            print("new Chapter: \(value)")
        })
        
    }
}

struct ChapterPicker: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var bibleStore: BibleStore
    @State var displayAlone = true
    @ObservedObject var activeSheet = SheetNavigator()
    
    let columns = [
        GridItem(.flexible(), spacing: 1),
        GridItem(.flexible(), spacing: 1),
        GridItem(.flexible(), spacing: 1),
        GridItem(.flexible(), spacing: 1),
        GridItem(.flexible(), spacing: 1),
        GridItem(.flexible(), spacing: 1),
        GridItem(.flexible(), spacing: 1),
        GridItem(.flexible(), spacing: 1),
        GridItem(.flexible(), spacing: 1),
        GridItem(.flexible(), spacing: 1)
    ]
    
    var body: some View {
        
            LazyVGrid(columns: columns, spacing: 0) {
                ForEach(1...bibleStore.chapters, id: \.self) { index in
                    Rectangle()
                        .aspectRatio(1.0, contentMode: .fit)
                        .foregroundColor(index == bibleStore.chapter ? Color.green: Color.gray)
                        .frame(height: 50)
                        .overlay(
                            // 6
                            
                            Button("\(index)", action: {
                                bibleStore.chapter = index
                                activeSheet.selectedSheet = .none
                                self.presentationMode.wrappedValue.dismiss()
//                                self.presentationMode.wrappedValue.dismiss()
                            }).foregroundColor(.white)
                        )
                }
            }
        
    }
}

struct SearchView: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var bibleStore: BibleStore
    @ObservedObject var activeSheet = SheetNavigator()
    @State var searchEditing = false
    
    var body: some View {
        VStack {
            HStack {
            Text("Search")
                .font(.largeTitle)
                .bold()
                .padding()
                Spacer()
            }
        Form {
            Section(header: Text("Found: \(bibleStore.searchResults.count)"), content: {
                ScrollView {
                    ForEach(0..<bibleStore.searchResults.count, id: \.self) { index in
                        Button(action: {
                            let components = bibleStore.searchResults[index].chapterId.components(separatedBy: ["."])
                            bibleStore.book = bibleStore.getBook(with: components[0])
                            bibleStore.chapter = Int(components[1]) ?? 1
//                            self.presentationMode.wrappedValue.dismiss()
                            activeSheet.selectedSheet = .none
                        }, label: {
                            VStack(alignment: .leading) {
                                HStack {
                                Text(bibleStore.searchResults[index].reference)
                                Spacer()
                                }
                                Text(bibleStore.searchResults[index].text)
                                    .lineLimit(1)
                                if index != (bibleStore.searchResults.count - 1) {
                                    Divider()
                                }
                            }
                        })
                    }
                }
            })
            .frame(maxHeight: searchEditing ? 240 : 500)
            .listStyle(PlainListStyle())
            
            TextField("Search String", text: $bibleStore.searchTerm) { isEditing in
                searchEditing = isEditing
            }
            onCommit: {
                bibleStore.performSearch()
                searchEditing = false
            }
        }
        
        }
        .padding(.bottom, 10)
        
    }
}


struct BibleVersion: Codable, Hashable, Identifiable {
    var id: String = ""
    var abbreviation: String = ""
    var name: String = ""
    var description: String = ""
    //store books in version so the query only needs to be done once.
//    var books: [BibleBook] = [BibleBook]()
}


struct BibleBook: Codable, Hashable {
    var id: String = ""
    var abbreviation: String = ""
    var name: String = ""
    var chapters: Int = 1
}

struct BibleBooks: Codable {
    var list: [BibleBook] = [BibleBook(id: "GEN", abbreviation: "Gen.", name: "Genesis", chapters: 50),BibleBook(id: "EXO", abbreviation: "Exo.", name: "Exodus", chapters: 40),BibleBook(id: "LEV", abbreviation: "Lev.", name: "Leviticus", chapters: 27),BibleBook(id: "NUM", abbreviation: "Num.", name: "Numbers", chapters: 50),BibleBook(id: "DEU", abbreviation: "Deu.", name: "Deuteronomy", chapters: 50),BibleBook(id: "JDG", abbreviation: "Jdg.", name: "Judges", chapters: 50)]
}

struct BibleText: Codable {
    var reference: String = ""
    var text: String = ""        // dictionary key content
}

struct SearchCollection: Hashable, Identifiable {
    var id: String = ""
    var results: [SearchResult] = [SearchResult]()
}

struct SearchResult: Hashable, Identifiable {
    var id: String = ""// "GEN.22.2"
    var bibleId: String = "" // "6bab4d6c61b31b80-01",
    var chapterId: String = "" //"GEN.22",
    var reference: String = "" //"Genesis 22:2",
    var text: String = "" //"And he said, Take thy son, the beloved one, whom thou hast loved—Isaac, and go into the high land, and offer him there for a whole burnt offering on one of the mountains which I will tell thee of."
}

struct BibleAPI_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            VersionPicker(bibleStore: BibleStore(lang: "eng", key: "52db9917134442d8559cbab5cc5e18f6"))
        }
    }
}
