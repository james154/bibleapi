//
//  ScriptureView.swift
//  BibleAPI
//
//  Created by James Sumners on 10/12/20.
//

import SwiftUI
import WebKit

struct ScriptureView: View {
    @Environment(\.colorScheme) var colorScheme
    @State var passage: NSAttributedString
    @ObservedObject var bibleStore: BibleStore
    @State var startPos: CGPoint = .zero
    @State var isSwipping = true
    @Binding var showUI: Bool
    @State var searchTerm: String
    
    var body: some View {
        ZStack {
            
            TextView(colorScheme: colorScheme, attributedString: $passage, showUI: $showUI, selectedText: $searchTerm)
                .padding(.leading, 5)
            
        }
        .gesture(DragGesture()
                    .onChanged { gesture in
                        if isSwipping {
                            startPos = gesture.location
                            isSwipping.toggle()
                        }
                    }
                    .onEnded { gesture in
                        let xDist =  abs(gesture.location.x - self.startPos.x)
                        let yDist =  abs(gesture.location.y - self.startPos.y)
                        if self.startPos.x > gesture.location.x && yDist < xDist {
                            self.bibleStore.chapter += 1
                            showUI = false
                        }
                        else if self.startPos.x < gesture.location.x && yDist < xDist {
                            if self.bibleStore.chapter > 1 {
                                self.bibleStore.chapter -= 1
                            }
                            showUI = false
                        }
                        self.isSwipping.toggle()
                    })
        .onChange(of: bibleStore.passage, perform: { value in
            print("Detect Passage Change")
            self.passage = value
        })
        .onChange(of: bibleStore.translation, perform: { value in
            bibleStore.loadScripture()
        })
        .onChange(of: bibleStore.book, perform: { value in
            bibleStore.loadScripture()
        })
        .onChange(of: bibleStore.chapter, perform: { value in
            bibleStore.loadScripture()
        })
        .onChange(of: searchTerm, perform: { value in
            bibleStore.searchTerm = value
        })
        .onAppear(perform: {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                bibleStore.loadScripture()
            }
        })
    }
    
    init(store: BibleStore, showUI: Binding<Bool>) {
        _bibleStore = ObservedObject(initialValue: store)
        _passage = State(initialValue: store.passage)
        _showUI = showUI
        _searchTerm = State(initialValue: store.searchTerm)
    }
}

struct BiblePassage: Decodable {
    let id: String
    let bibleId: String
    let orgId: String
    let content: String
    let reference: String
    let verseCount: Int
    let copyright: String
}

struct Meta: Decodable {
    let fums: String
    let fumsId: String
    let fumsJsIclude: String
    let fumsJs: String
    let fumsNoScript: String
}

struct BibleJSON: Decodable {
    let data: BiblePassage
}

struct TextChanges {
    let fontSize: Int
    let verseSize: Int
    let fontName: String
    let text: String
    
    init(attrString: NSAttributedString) {
        if attrString.length > 0 {
            if let textFont = attrString.attribute(.font, at: 1, effectiveRange: nil) as? UIFont {
                self.fontSize = Int(textFont.pointSize)
                self.fontName = textFont.familyName
            }
            else {
                self.fontSize = 22
                self.fontName = "Baskerville"
            }
            if let verseFont = attrString.attribute(.font, at: 0, effectiveRange: nil) as? UIFont {
                self.verseSize = Int(verseFont.pointSize)
            }
            else {
                self.verseSize = 13
            }
            self.text = attrString.string
        }
        else {
            self.fontSize = 22
            self.fontName = "Baskerville"
            self.verseSize = 13
            self.text = ""
        }
    }
    
    func isEqual(_ testText: TextChanges) -> Bool {
        if self.fontName != testText.fontName || self.fontSize != testText.fontSize ||
            self.text != testText.text || self.verseSize != testText.verseSize {
            return false
        }
        else{
            return true
        }
    }
}


struct TextView: UIViewRepresentable {
    @State var colorScheme: ColorScheme
    @Binding var attributedString: NSAttributedString
    @Binding var showUI: Bool
    @Binding var selectedText: String
    
    func makeUIView(context: Context) -> UITextView {
        let textView = UITextView()
        textView.attributedText = attributedText() //attributedText()
        textView.isEditable = false
        
        textView.delegate = context.coordinator
        
        return textView
    }
    
    func updateUIView(_ textView: UITextView, context: Context) {
            let currentText = TextChanges(attrString: textView.attributedText)
            let newText = TextChanges(attrString: attributedText())
            
            if currentText.isEqual(newText) == false {
                textView.attributedText = attributedText()
                
                textView.selectedRange = NSRange(location: 0, length: 0)
                
                textView.setContentOffset(.zero, animated: true)
            }
        
    }
    
    func attributedText() -> NSMutableAttributedString {
        
        
        let mutAttrString = NSMutableAttributedString(attributedString: attributedString)
        
        mutAttrString.addAttribute(.foregroundColor, value: UIColor(Color("bibleMenuLabel")), range: NSRange(location: 0, length: mutAttrString.length))
        
        return mutAttrString
    }

    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    class Coordinator: NSObject, UITextViewDelegate {
        var parent: TextView
        
        init(_ textView: TextView) {
            self.parent = textView
        }

        func scrollViewDidScroll(_ scrollView: UIScrollView) {
            if self.parent.showUI {
                DispatchQueue.main.async {
                    self.parent.showUI = false
                }
            }
            
        }
        
        func textViewDidChangeSelection(_ textView: UITextView) {
            if self.parent.selectedText != textView.attributedText.attributedSubstring(from: textView.selectedRange).string &&
                textView.selectedRange.length > 0 {
                self.parent.selectedText = textView.attributedText.attributedSubstring(from: textView.selectedRange).string
            }
        }
    }
}

struct ScriptureView_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            NavigationView {
                ScriptureView(store: BibleStore(lang: "eng", key: "52db9917134442d8559cbab5cc5e18f6"), showUI: .constant(true))
                    .navigationTitle("Genesis 1")
            }
        }
    }
}
