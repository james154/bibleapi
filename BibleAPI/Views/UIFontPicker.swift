//
//  UIFontPicker.swift
//  BibleAPI
//
//  Created by James Sumners on 2/5/21.
//

import SwiftUI

struct UIFontPicker: View {
    @Binding var font: UIFont
    @State var fonts: [UIFont]
    var body: some View {
        Picker("Font \(font.familyName)", selection: $font) {
            ForEach(0..<fonts.count) { index in
                Text(fonts[index].familyName)
                    .font(fonts[index].font)
                    .tag(UIFont(name: fonts[index].familyName, size: font.pointSize))
            }
        }
        .onChange(of: font, perform: { value in
            print("font change \(font.familyName)")
        })
        .font(font.font)
        .navigationTitle("Font: \(font.fontName)")
        
    }
    
    init(uiFont: Binding<UIFont>) {
        _font = uiFont
        
        var tempFonts = [UIFont]()
        for fontItem in UIFont.familyNames.sorted() {
            if let tempFont = UIFont(name: fontItem, size: CGFloat(17)) {
                tempFonts.append(tempFont)
            }
        }
        
        _fonts = State(initialValue: tempFonts)
    }
}

struct UIFontPicker_Previews: PreviewProvider {
    static var previews: some View {
        UIFontPicker(uiFont: .constant( UIFont(name: "Helvetica", size: CGFloat(17)) ?? UIFont()))
    }
}
