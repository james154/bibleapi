//
//  BibleSettings.swift
//  BibleAPI
//
//  Created by James Sumners on 2/5/21.
//

import SwiftUI

let selectFonts = ["Avenir",
                   "Arial",
                   "American Typewriter",
                   "Baskerville",
                   "Cochin",
                   "Didot",
                   "Helvetica Neue",
                   "Palatino",
                   "Times New Roman"]

struct BibleSettings: View {
    @State var font: UIFont = UIFont()
    @State var textSize: Int
    @State var verseSize: Int
    @ObservedObject var bibleStore: BibleStore
    @State var fonts: [UIFont]
    @State var showFonts = false
    var body: some View {
        NavigationView {
            VStack {
                List {
                    DisclosureGroup(
                        isExpanded: $showFonts,
                        content: {
                            ForEach(0..<fonts.count) { index in
                                Button(action: {
                                    font = UIFont(name: fonts[index].familyName, size: font.pointSize) ?? font
                                    showFonts = false
                                }, label: {
                                    HStack {
                                        Image(systemName: "checkmark")
                                            .foregroundColor(font.familyName == fonts[index].familyName ? Color("bibleMenuLabel") : Color.clear)
                                        Text(fonts[index].familyName)
                                    }
                                })
                                .font(fonts[index].font)
                            }
                            
                        },
                        label: { Text("Font \(font.familyName)")
                            .onTapGesture(perform: {
                                self.showFonts = true
                            })
                            
                        })
                    .font(font.font)
                    .frame(maxHeight: 80)
                    Stepper(value: $textSize, in: 15...40) {
                        Text("Text Size \(textSize)")
                            .font(font.font)
                    }
                    Stepper(value: $verseSize, in: 10...20) {
                        Text("Verse Size \(verseSize)")
                            .font(UIFont(name: font.familyName, size: CGFloat(verseSize))?.font)
                    }
                    
                    Section(header: Text("Bible Settings")) {
                        Text("Preferred Bible")
                        Text("Language")
                        Text("Prefered App")
                    }
                }
                .onChange(of: font, perform: { value in
                    bibleStore.font = value
                    print("UIFontPicker font changed to \(value.familyName)")
                    bibleStore.updateScripture()
                })
                .onChange(of: textSize, perform: { value in
                    bibleStore.font = UIFont(name: font.fontName, size: CGFloat(value)) ?? bibleStore.font
                    bibleStore.updateScripture()
                })
                .onChange(of: verseSize, perform: { value in
                    bibleStore.verseSize = value
                    bibleStore.updateScripture()
                })
            }
            .navigationTitle("Settings")
        }
    }
    
    init(store: BibleStore) {
        _bibleStore = ObservedObject(initialValue: store)
        _font = State(initialValue: store.font)
        _textSize = State(initialValue: store.fontSize)
        _verseSize = State(initialValue: store.verseSize)
        var tempFonts = [UIFont]()
        for fontItem in selectFonts.sorted() {
            if let tempFont = UIFont(name: fontItem, size: CGFloat(17)) {
                tempFonts.append(tempFont)
            }
        }
        
        _fonts = State(initialValue: tempFonts)
    }
}

struct BibleSettings_Previews: PreviewProvider {
    static var previews: some View {
        BibleSettings(store: BibleStore(lang: "eng", key: "52db9917134442d8559cbab5cc5e18f6"))
    }
}
