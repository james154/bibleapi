//
//  ContentView.swift
//  BibleAPI
//
//  Created by James Sumners on 10/12/20.
//

import SwiftUI
import CoreData
import CoreSpotlight
import MobileCoreServices

struct ContentView: View {
    @Environment(\.managedObjectContext) private var viewContext
    
    @ObservedObject var bibleStore: BibleStore
    @State var uiAlignment = Alignment.bottomTrailing
    @State var showUI = true
    @State var animationRate = 0.25
    
    var body: some View {
        ZStack(alignment: uiAlignment) {
            ScriptureView(store: bibleStore, showUI: $showUI)
                .onTapGesture {
                    showUI.toggle()
                }
                .navigationTitle(bibleStore.title)
                .onContinueUserActivity("BibleBLeaf.BibleAPI.reference", perform: { userActivity in
                    let test = userActivity.referrerURL
//                    print(test)
                })
            if showUI {
                VersePickerSide(bibleStore: bibleStore, pickerAlignment: $uiAlignment.horizontal, showMe: $showUI)
                    .animation(.easeInOut(duration: animationRate))
            }
        }
        .ignoresSafeArea(edges: .bottom)
        .userActivity("BibleBLeaf.BibleAPI.reference", { activity in
            bibleStore.loadBooks()
            var bookList = [String]()
            for bk in bibleStore.books.list {
                bookList.append(bk.name)
            }
            
            activity.isEligibleForSearch = true
            activity.title = "Bible Reference and Search"
            activity.userInfo = ["bibleref": bibleStore.translation.id]
            
            let attributes = CSSearchableItemAttributeSet(itemContentType: kUTTypeItem as String)
            
            attributes.contentDescription = "Go to Bible Reference"
            attributes.keywords = bookList
        })
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView(bibleStore: BibleStore(lang: "eng", key: "52db9917134442d8559cbab5cc5e18f6")).environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
        }
    }
}
